package com.baroody.mahmoudalbaroodytask.adapter

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.baroody.mahmoudalbaroodytask.R
import com.baroody.mahmoudalbaroodytask.model.FlatResponseItem
import com.baroody.mahmoudalbaroodytask.ui.main.musicList.MainFragment
import com.baroody.mahmoudalbaroodytask.ui.main.musicList.OnItemMusicClick
import com.baroody.mahmoudalbaroodytask.utils.getBitmapFromURL
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class MusicAdapter(
    private val mActivity: Activity,
     val flat: ArrayList<FlatResponseItem>,
    fragment: MainFragment
) :
    RecyclerView.Adapter<MusicAdapter.ViewHolder>() {

    private val onItemMusicClick: OnItemMusicClick = fragment
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(mActivity).inflate(R.layout.music_item, parent, false))


    override fun getItemCount(): Int = flat.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.setImageBitmap(flat[position].cover?.defaultBitmap)
        holder.titlee.text = flat[position].title
        holder.type.text = flat[position].type
        holder.artist.text = flat[position].mainArtist?.name
        holder.itemMusic.setOnClickListener { onItemMusicClick.onItemMusicClick(flat[position]) }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.imageView)
        val titlee: TextView = view.findViewById(R.id.titlee)
        val type: TextView = view.findViewById(R.id.type)
        val artist: TextView = view.findViewById(R.id.artist)
        val itemMusic: CardView = view.findViewById(R.id.item_music)

    }


}