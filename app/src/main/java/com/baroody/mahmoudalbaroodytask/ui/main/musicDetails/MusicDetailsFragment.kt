package com.baroody.mahmoudalbaroodytask.ui.main.musicDetails

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.core.view.isVisible
import com.baroody.mahmoudalbaroodytask.R
import com.baroody.mahmoudalbaroodytask.model.FlatResponseItem
import com.baroody.mahmoudalbaroodytask.utils.getBitmapFromURL
import com.google.android.material.appbar.AppBarLayout

class MusicDetailsFragment : Fragment() {

    lateinit var view2: View



    private lateinit var viewModel: MusicDetailsViewModel
    lateinit var item: FlatResponseItem
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        item = arguments?.getSerializable("item") as FlatResponseItem
        view2 = inflater.inflate(R.layout.music_details_fragment, container, false)
        return view2
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MusicDetailsViewModel::class.java)
        // TODO: Use the ViewModel
        requireActivity().findViewById<AppBarLayout>(R.id.search).isVisible = false
        setData()

    }

    private fun setData() {
        view2.findViewById<ImageView>(R.id.music_image).setImageBitmap(item.cover?.defaultBitmap)
        view2.findViewById<TextView>(R.id.music_title).text = item.title
        view2.findViewById<TextView>(R.id.music_type).text = item.type
        view2.findViewById<TextView>(R.id.name_artist).text = item.mainArtist?.name
    }

}