package com.baroody.mahmoudalbaroodytask.ui.main.musicList


import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.baroody.mahmoudalbaroodytask.utils.executeAsyncTask
import com.baroody.mahmoudalbaroodytask.model.*
import com.baroody.mahmoudalbaroodytask.repo.Repository
import com.baroody.mahmoudalbaroodytask.utils.ConsumableLiveData
import com.baroody.mahmoudalbaroodytask.utils.Resource


class MainViewModel constructor(private val repository: Repository)  : ViewModel() {
    private val _getMusicResponse= ConsumableLiveData<Resource<ResponseModel>>()
    val getMusicResponse: ConsumableLiveData<Resource<ResponseModel>>
        get() = _getMusicResponse

    private val _clientResponse = ConsumableLiveData<Resource<ResponseModel>>()
    val clientResponse: ConsumableLiveData<Resource<ResponseModel>>
        get() = _clientResponse


    fun getClient() {
        viewModelScope.executeAsyncTask(onPreExecute = {
            // ... runs in Main Thread
        }, doInBackground = {
            // ... runs in IO Thread
            repository.getClientCall()
        }, onPostExecute = {
            it?.let {
                _clientResponse.postValue(it)
                _clientResponse.consume = true
            }
            // runs in Main Thread
            // ... here "it" is the data returned from "doInBackground"
        })
    }

    fun getMusic(searchKey: String, clientModel: ClientModel) {
        viewModelScope.executeAsyncTask(onPreExecute = {
            // ... runs in Main Thread
        }, doInBackground = {
                repository.getMusicCall(
                    "http://staging-gateway.mondiamedia.com/v2/api/sayt/flat?query=$searchKey&limit=20",
                    clientModel
                )
        }, onPostExecute = {
            _getMusicResponse.postValue(it)
            _getMusicResponse.consume = true

            // runs in Main Thread
            // ... here "it" is the data returned from "doInBackground"
        })
    }
}
