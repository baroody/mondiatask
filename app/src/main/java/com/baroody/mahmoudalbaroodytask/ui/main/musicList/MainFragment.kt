package com.baroody.mahmoudalbaroodytask.ui.main.musicList

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.baroody.mahmoudalbaroodytask.R
import com.baroody.mahmoudalbaroodytask.adapter.MusicAdapter
import com.baroody.mahmoudalbaroodytask.model.ClientModel
import com.baroody.mahmoudalbaroodytask.model.Cover
import com.baroody.mahmoudalbaroodytask.model.FlatResponseItem
import com.baroody.mahmoudalbaroodytask.model.MainArtist
import com.baroody.mahmoudalbaroodytask.repo.MyViewModelFactory
import com.baroody.mahmoudalbaroodytask.repo.Repository
import com.baroody.mahmoudalbaroodytask.utils.getBitmapFromURL
import com.baroody.mahmoudalbaroodytask.utils.hasInternet
import com.baroody.mahmoudalbaroodytask.utils.retrieveToken
import com.baroody.mahmoudalbaroodytask.utils.saveToken
import com.google.android.material.appbar.AppBarLayout
import org.json.JSONArray
import org.json.JSONObject


class MainFragment : Fragment(), OnItemMusicClick {
    var searchWords = ""
    var page = 0
    lateinit var search: SearchView
    var preference: SharedPreferences? = null
    lateinit var clientModel: ClientModel
    private val musicList = ArrayList<FlatResponseItem>()
    private lateinit var mainArtist: JSONObject
    lateinit var cover: JSONObject
    var coverObject: Cover? = null
    var mainArtistObject: MainArtist? = null
    private lateinit var viewModel: MainViewModel
    lateinit var musicAdapter: MusicAdapter
    lateinit var view1: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        view1 = inflater.inflate(R.layout.main_fragment, container, false)

        return view1
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        requireActivity().findViewById<AppBarLayout>(R.id.search).isVisible = true
        musicAdapter = MusicAdapter(requireActivity(), musicList, this)
        view1.findViewById<RecyclerView>(R.id.rv).adapter = musicAdapter

        makeSearch()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preference = requireActivity().getSharedPreferences(
            resources.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
        viewModel =
            ViewModelProvider(this, MyViewModelFactory(Repository())).get(MainViewModel::class.java)

        if (hasInternet(requireContext())) {
            viewModel.getClient()
        } else {
            Toast.makeText(requireContext(), "No Internet", Toast.LENGTH_LONG).show()
        }

        viewModel.getMusicResponse.observe(requireActivity(), {
            when (it.data?.code) {
                200 -> {
                    handleMusicList(it.data.response)
                }
            }
        })

        viewModel.clientResponse.observe(requireActivity(), {
            when (it.data?.code) {
                200 -> {
                    handleClient(it.data.response)
                }
                400 -> {
                  Log.d("log","REQUEST_VALIDATION_ERROR")
                }
                500 -> {
                    Log.d("log","INTERNAL_SERVER_ERROR")
                }
            }
        })


    }

    private fun handleMusicList(it: String?) {
        musicList.clear()
        val json = JSONArray(it)
        for (i in 0 until json.length()) {
            val jsonObject: JSONObject = json.getJSONObject(i)
            val id = jsonObject.getInt("id")
            val type = jsonObject.getString("type")
            var title = ""
            if (jsonObject.has("title")) {
                title = jsonObject.getString("title")
            }

            if (jsonObject.has("mainArtist")) {
                mainArtist = jsonObject.getJSONObject("mainArtist")
                var id1 = 0
                if (mainArtist.has("id")) {
                    id1 = mainArtist.getInt("id")
                }
                val ma2 = mainArtist.getString("name")
                mainArtistObject = MainArtist(id1, ma2)
            }



            if (jsonObject.has("cover")) {
                mainArtist = jsonObject.getJSONObject("cover")
                val cover = jsonObject.getJSONObject("cover")
                val tiny = cover.getString("tiny")
                val small = cover.getString("small")
                val medium = cover.getString("medium")
                val large = cover.getString("large")
                val default = cover.getString("default")
                val template = cover.getString("template")
                Thread {
                    val defaultBitmap = getBitmapFromURL(default.replace("//", "https://"))
                    coverObject = defaultBitmap?.let { it1 ->
                        Cover(tiny, small, medium, large, default, template, it1)
                    }
                    musicList.add(
                        FlatResponseItem(
                            coverObject,
                            id,
                            title,
                            type,
                            mainArtistObject,
                        )
                    )
                    requireActivity().runOnUiThread { musicAdapter.notifyDataSetChanged() }

                }.start()
            }
        }
    }

    private fun handleClient(it: String) {
        clientModel = getClientModel(it)
        if (preference != null) {
            saveToken(preference!!, clientModel)
            viewModel.getMusic("ru", clientModel)
        }
    }

    private fun getClientModel(it: String): ClientModel {
        val jsonObject = JSONObject(it)
        return ClientModel(
            jsonObject.getString("accessToken"),
            jsonObject.getString("tokenType"),
            jsonObject.getString("expiresIn")
        )
    }

    private fun makeSearch() {
        search = requireActivity().findViewById(R.id.search)
        search.setQuery("", false)
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchWords = query.toString()
                page = 0
                when {
                    searchWords.length < 2 -> {
                        Toast.makeText(
                            requireContext(),
                            "you should enter word contain 2 digits",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                    else -> {
                        viewModel.getMusic(searchWords, retrieveToken(preference!!))
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.isNullOrEmpty() && searchWords != "") {
                    searchWords = ""
                    page = 0
                    if (searchWords.length >= 2) {
                        viewModel.getMusic(searchWords, retrieveToken(preference!!))
                    }
                }
                return false
            }
        })
    }


    override fun onItemMusicClick(item: FlatResponseItem) {
        findNavController().navigate(
            R.id.action_nav_mainFragment_to_nav_login,
            bundleOf("item" to item)
        )
    }

}