package com.baroody.mahmoudalbaroodytask.ui.main.musicList

import com.baroody.mahmoudalbaroodytask.model.FlatResponseItem

interface OnItemMusicClick {
    fun onItemMusicClick(item:FlatResponseItem)
}