package com.baroody.mahmoudalbaroodytask.utils


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo


fun hasInternet(context: Context?) =
    (context?.getSystemService(Context.CONNECTIVITY_SERVICE) as
            ConnectivityManager?)?.allNetworkInfo?.firstOrNull {
        it.state == NetworkInfo.State.CONNECTED
    } != null