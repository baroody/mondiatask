package com.baroody.mahmoudalbaroodytask.utils

import android.content.SharedPreferences
import com.baroody.mahmoudalbaroodytask.model.ClientModel

fun saveToken(preference: SharedPreferences, clientModel: ClientModel) {
    val editor = preference.edit()
    editor.putString("accessToken", clientModel.accessToken)
    editor.putString("tokenType", clientModel.tokenType)
    editor.putString("expiresIn", clientModel.expiresIn)
    editor.apply()
}

fun retrieveToken(preference: SharedPreferences): ClientModel {
    val accessToken = preference.getString("accessToken", "")
    val tokenType = preference.getString("tokenType", "")
    val expiresIn = preference.getString("expiresIn", "")
    return ClientModel(accessToken ?: "",
        tokenType ?: "",
        expiresIn ?: "")
}