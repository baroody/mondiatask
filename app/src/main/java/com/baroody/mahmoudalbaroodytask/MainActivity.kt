package com.baroody.mahmoudalbaroodytask

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.navigation.NavController
import androidx.navigation.findNavController

import com.baroody.mahmoudalbaroodytask.ui.main.musicList.MainFragment


class MainActivity : AppCompatActivity() {

    lateinit var search: SearchView
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        search = findViewById(R.id.search)
        navController = findNavController(R.id.nav_host_fragment)
        val navInflater = navController.navInflater
        val navGraph = navInflater.inflate(R.navigation.mobile_navigation)
        navGraph.startDestination = R.id.nav_mainFragment

    }

    fun onSearchClick(view: View) {
        val im = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        im.showSoftInput(search, 0)
        search.onActionViewExpanded()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (navController.currentDestination?.id == R.id.nav_musicDetailsFragment ){
            navController.popBackStack()
        }
    }

}