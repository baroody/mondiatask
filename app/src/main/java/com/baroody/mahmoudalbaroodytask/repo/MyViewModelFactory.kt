package com.baroody.mahmoudalbaroodytask.repo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.baroody.mahmoudalbaroodytask.ui.main.musicList.MainViewModel

class MyViewModelFactory constructor(private val repository:Repository): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            MainViewModel(this.repository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}