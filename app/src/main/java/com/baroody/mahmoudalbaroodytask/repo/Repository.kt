package com.baroody.mahmoudalbaroodytask.repo

import android.util.Log
import com.baroody.mahmoudalbaroodytask.model.ClientModel
import com.baroody.mahmoudalbaroodytask.model.ResponseModel
import com.baroody.mahmoudalbaroodytask.utils.Resource
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class Repository {
     fun getClientCall():Resource<ResponseModel>?  {
         var response:Resource<ResponseModel>?=null
        val connection = URL("http://staging-gateway.mondiamedia.com/v0/api/gateway/token/client").openConnection()
                as HttpURLConnection
        try {
            connection.requestMethod = "POST"
            connection.doInput = true
            connection.doOutput = false
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
            connection.setRequestProperty(
                "X-MM-GATEWAY-KEY",
                "G2269608a-bf41-2dc7-cfea-856957fcab1e"
            )

            connection.inputStream.bufferedReader().use {
                response =
                    Resource.Success(ResponseModel(it.readText(), connection.responseCode))
            }
        } catch (e:Exception){

        }
        finally {
            connection.disconnect()
        }
        return response
    }
     fun getMusicCall(
        requestURL: String, clientModel: ClientModel
    ): Resource<ResponseModel>? {

        var response:Resource<ResponseModel>?=null
        val connection = URL(requestURL).openConnection() as HttpURLConnection
        try {
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.doOutput = false
            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("Accept", "application/json")
            connection.setRequestProperty("Authorization", "Bearer ${clientModel.accessToken}")

            connection.inputStream.bufferedReader().use {
                    response =
                        Resource.Success(ResponseModel(it.readText(), connection.responseCode))
            }

            // ... do something with "data"
        } catch (e: Exception) {

        } finally {
            connection.disconnect()
        }

        return response
    }


    private fun grouped(
        requestURL: String?
    ): String? {
        val token =
            "C3375120e-8229-49e5-9864-6018ac7c16a5"
        var response: String? = ""

        val connection = URL(requestURL).openConnection() as HttpURLConnection
        try {
            connection.requestMethod = "GET"
            connection.doInput = true
            connection.doOutput = false

            connection.setRequestProperty("Content-Type", "application/json")
            connection.setRequestProperty("Accept", "application/json")
            connection.setRequestProperty("Authorization", "Bearer $token")

            //   val postData: ByteArray = message.toByteArray(StandardCharsets.UTF_8)

//            try {
//                val outputStream = OutputStreamWriter(connection.outputStream)
//                outputStream.write(reqParam)
//                outputStream.flush()
//            } catch (exception: Exception) {
//
//            }
            connection.inputStream.bufferedReader().use {
                val json = JSONArray(it.readText())
                for (i in 0 until json.length()) {
                    val jsonobject: JSONObject = json.getJSONObject(i)
                    val id = jsonobject.getInt("groupCount")
                    Log.d("Pretty Printed JSON :", id.toString())
                }
                connection.responseCode
                Log.e("ddd", connection.responseCode.toString())

            }
            // ... do something with "data"
        } finally {
            connection.disconnect()
        }

        return response
    }

}