package com.baroody.mahmoudalbaroodytask.model

import java.io.Serializable

data class MainArtist(
    val id: Int,
    val name: String
): Serializable