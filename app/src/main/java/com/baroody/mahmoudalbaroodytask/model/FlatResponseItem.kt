package com.baroody.mahmoudalbaroodytask.model

import java.io.Serializable

data class FlatResponseItem(
    val cover: Cover?,
    val id: Int,
    val title: String?,
    val type: String,
    val mainArtist: MainArtist?
) : Serializable