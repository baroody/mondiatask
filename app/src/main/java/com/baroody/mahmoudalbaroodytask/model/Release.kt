package com.baroody.mahmoudalbaroodytask.model

data class Release(
    val id: Int,
    val title: String
)