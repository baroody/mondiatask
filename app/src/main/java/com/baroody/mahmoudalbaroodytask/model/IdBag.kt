package com.baroody.mahmoudalbaroodytask.model

data class IdBag(
    val grid: String,
    val isrc: String
)