package com.baroody.mahmoudalbaroodytask.model

data class ClientModel(
    val accessToken: String,
    val tokenType: String,
    val expiresIn: String
)
