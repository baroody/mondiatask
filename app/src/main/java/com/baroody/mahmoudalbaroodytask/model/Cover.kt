package com.baroody.mahmoudalbaroodytask.model

import android.graphics.Bitmap
import java.io.Serializable

data class Cover(
    val tiny: String,
    val small: String,
    val medium: String,
    val large: String,
    val default: String,
    val template: String,
    val defaultBitmap:Bitmap
): Serializable
