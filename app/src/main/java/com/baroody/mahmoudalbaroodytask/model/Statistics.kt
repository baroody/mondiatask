package com.baroody.mahmoudalbaroodytask.model

data class Statistics(
    val estimatedRecentCount: Int,
    val estimatedTotalCount: Int,
    val popularity: String
)